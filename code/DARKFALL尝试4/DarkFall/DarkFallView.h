// DarkFallView.h : interface of the CDarkFallView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_DARKFALLVIEW_H__97977436_5031_4A4D_9379_934ED71BAC1B__INCLUDED_)
#define AFX_DARKFALLVIEW_H__97977436_5031_4A4D_9379_934ED71BAC1B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CDarkFallView : public CView
{
protected: // create from serialization only
	CDarkFallView();
	DECLARE_DYNCREATE(CDarkFallView)

// Attributes
public:
	CDarkFallDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDarkFallView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CDarkFallView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CDarkFallView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in DarkFallView.cpp
inline CDarkFallDoc* CDarkFallView::GetDocument()
   { return (CDarkFallDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DARKFALLVIEW_H__97977436_5031_4A4D_9379_934ED71BAC1B__INCLUDED_)
