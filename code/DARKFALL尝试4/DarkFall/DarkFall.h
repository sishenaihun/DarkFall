// DarkFall.h : main header file for the DARKFALL application
//

#if !defined(AFX_DARKFALL_H__52A0645D_22DA_4D46_A27A_69EECDAEE58B__INCLUDED_)
#define AFX_DARKFALL_H__52A0645D_22DA_4D46_A27A_69EECDAEE58B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CDarkFallApp:
// See DarkFall.cpp for the implementation of this class
//

class CDarkFallApp : public CWinApp
{
public:
	CDarkFallApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDarkFallApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CDarkFallApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DARKFALL_H__52A0645D_22DA_4D46_A27A_69EECDAEE58B__INCLUDED_)
