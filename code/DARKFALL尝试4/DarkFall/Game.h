// Game.h: interface for the Game class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GAME_H__0A4211DE_A88A_41B6_9CC7_54813C5E44D8__INCLUDED_)
#define AFX_GAME_H__0A4211DE_A88A_41B6_9CC7_54813C5E44D8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Element.h"

class Game
{
public:
	Game(void);
	~Game(void);
	int SetTop(int top);
	int SetLeft(int left);
	int SetLength(int length);
	CWnd * SetCWnd(CWnd * pWnd);
	bool SetSelect(bool select);
	void Set(int top, int left);
	void Show();
	CBrush * CreateColor(/*int &i*/);
	void CreateElement(void);
	void OnLButtonDown(CPoint point);

private:
	int m_top;
	int m_left;
	int m_length;
	CWnd * m_pWnd;
	bool m_select;
	int m_lastX;
	int m_lastY;
	Element (*pElement)[8];	

public:
	void Swap(int x, int y, int last_x, int last_y);
	int Checkout(int x, int y, int s);
	int CheckoutX(int x, int y, int s);
	int CheckoutY(int x, int y, int s);
	void Descend(void);
	void Scan(void);
	int Over(void);
	void Text(CDC * pDC);
private:
	int m_carom;
	int m_caromMax;
	int m_score;
public:
	int GetCarom(void);
	int GetCaromMax(void);
	int GetScore(void);
	void OnLButtonUp(CPoint point);
private:
	CPoint m_point;
public:
	int AddScore(int num);
	void AddCarom(void);
	int ReCarom(void);
	int ReCaromMax(void);
};

#endif // !defined(AFX_GAME_H__0A4211DE_A88A_41B6_9CC7_54813C5E44D8__INCLUDED_)
