// Element.h: interface for the Element class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ELEMENT_H__E7048363_0859_44BF_BCFE_D0A83750AE09__INCLUDED_)
#define AFX_ELEMENT_H__E7048363_0859_44BF_BCFE_D0A83750AE09__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class Element
{
public:
	Element(void);
	~Element(void);
	int SetTop(int top);
	int SetLeft(int left);
	int SetLength(int length);
	CWnd * SetCWnd(CWnd * pWnd);
	CBrush * SetCBrush(CBrush * pBColor);
	bool SetSelect(bool select);
	void Set(int top, int left, CBrush * pBColor);
	void Show(CDC * pDC);


private:
	int m_top;
	int m_left;
	int m_length;
	CWnd * m_pWnd;
	CBrush * m_pBColor;
	bool m_select;
	bool m_sign;


public:
	CBrush * GetBColor(void);
	bool Equal(Element element);
	bool GetSign(void);
	bool SetSign(bool sign);
};



#endif // !defined(AFX_ELEMENT_H__E7048363_0859_44BF_BCFE_D0A83750AE09__INCLUDED_)
