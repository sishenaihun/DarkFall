// Tool.h: interface for the Tool class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TOOL_H__B4DAE4DE_BDBB_4CCF_B4BE_964CB01A493C__INCLUDED_)
#define AFX_TOOL_H__B4DAE4DE_BDBB_4CCF_B4BE_964CB01A493C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class Tool
{
public:
	Tool(void);
	~Tool(void);
	CPen *black_pen;
	CPen *white_pen;
	CPen *fuchsia_pen;
	CBrush *black_brush;
	CBrush *white_brush;
	CBrush *blue_brush;
	CBrush *green_brush;
	CBrush *red_brush;
	CBrush *yellow_brush;
	CBrush *gray_brush;
	CBrush *buff_brush;
};


#endif // !defined(AFX_TOOL_H__B4DAE4DE_BDBB_4CCF_B4BE_964CB01A493C__INCLUDED_)
