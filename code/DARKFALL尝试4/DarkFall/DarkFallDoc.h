// DarkFallDoc.h : interface of the CDarkFallDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_DARKFALLDOC_H__0A49BBD8_D274_4A7B_94BF_3E229C452DC6__INCLUDED_)
#define AFX_DARKFALLDOC_H__0A49BBD8_D274_4A7B_94BF_3E229C452DC6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CDarkFallDoc : public CDocument
{
protected: // create from serialization only
	CDarkFallDoc();
	DECLARE_DYNCREATE(CDarkFallDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDarkFallDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CDarkFallDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CDarkFallDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DARKFALLDOC_H__0A49BBD8_D274_4A7B_94BF_3E229C452DC6__INCLUDED_)
