// DarkFallDoc.cpp : implementation of the CDarkFallDoc class
//

#include "stdafx.h"
#include "DarkFall.h"

#include "DarkFallDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDarkFallDoc

IMPLEMENT_DYNCREATE(CDarkFallDoc, CDocument)

BEGIN_MESSAGE_MAP(CDarkFallDoc, CDocument)
	//{{AFX_MSG_MAP(CDarkFallDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDarkFallDoc construction/destruction

CDarkFallDoc::CDarkFallDoc()
{
	// TODO: add one-time construction code here

}

CDarkFallDoc::~CDarkFallDoc()
{
}

BOOL CDarkFallDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CDarkFallDoc serialization

void CDarkFallDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CDarkFallDoc diagnostics

#ifdef _DEBUG
void CDarkFallDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CDarkFallDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDarkFallDoc commands
