// DarkFallView.cpp : implementation of the CDarkFallView class
//

#include "stdafx.h"
#include "DarkFall.h"

#include "DarkFallDoc.h"
#include "DarkFallView.h"

#include "Game.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

Game * g_Game;

/////////////////////////////////////////////////////////////////////////////
// CDarkFallView

IMPLEMENT_DYNCREATE(CDarkFallView, CView)

BEGIN_MESSAGE_MAP(CDarkFallView, CView)
	//{{AFX_MSG_MAP(CDarkFallView)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDarkFallView construction/destruction

CDarkFallView::CDarkFallView()
{
	// TODO: add construction code here

	g_Game = new Game();
	g_Game->Set(50,50);
	g_Game->CreateElement();
	g_Game->SetCWnd(this);	

}

CDarkFallView::~CDarkFallView()
{
	delete g_Game;
}

BOOL CDarkFallView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CDarkFallView drawing

void CDarkFallView::OnDraw(CDC* pDC)
{
	CDarkFallDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here

	g_Game->Show();
}

/////////////////////////////////////////////////////////////////////////////
// CDarkFallView diagnostics

#ifdef _DEBUG
void CDarkFallView::AssertValid() const
{
	CView::AssertValid();
}

void CDarkFallView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CDarkFallDoc* CDarkFallView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDarkFallDoc)));
	return (CDarkFallDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDarkFallView message handlers

void CDarkFallView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	g_Game->OnLButtonDown(point);
	g_Game->Show();

	CView::OnLButtonDown(nFlags, point);
}

void CDarkFallView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	g_Game->OnLButtonUp(point);
	g_Game->Show();

	CView::OnLButtonUp(nFlags, point);
}

void CDarkFallView::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	
	switch(nIDEvent)
	{
	case 1:
		{
			g_Game->ReCarom();
			g_Game->Show();
			break;
		}
	default:
		break;
	}

	CView::OnTimer(nIDEvent);
}
