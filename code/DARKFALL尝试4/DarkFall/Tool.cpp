// Tool.cpp: implementation of the Tool class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DarkFall.h"
#include "Tool.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Tool::Tool(void)
{
	black_pen = new CPen(PS_SOLID, 3, RGB(0,0,0));
	white_pen = new CPen(PS_SOLID, 3, RGB(234,234,234));
	fuchsia_pen = new CPen(PS_SOLID, 3, RGB(255,0,255));

	black_brush = new CBrush(RGB(0,0,0));
	white_brush = new CBrush(RGB(230,230,230));
	blue_brush = new CBrush(RGB(0,200,200));
	green_brush = new CBrush(RGB(0,200,0));
	red_brush = new CBrush(RGB(200,0,0));
	yellow_brush = new CBrush(RGB(200,200,0));
	gray_brush = new CBrush(RGB(128,128,128));
	buff_brush = new CBrush(RGB(128,128,0));
}


Tool::~Tool(void)
{
	delete black_pen;
	delete white_pen;
	delete fuchsia_pen;

	delete black_brush;
	delete white_brush;
	delete blue_brush;
	delete green_brush;
	delete red_brush;
	delete yellow_brush;
	delete gray_brush;
	delete buff_brush;
}
