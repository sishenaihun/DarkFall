#include <stdlib.h>
#include <iostream.h>
#include <time.h>
enum PhState{Thinking=0,Waiting,Eating};//哲学家状态
int stick[5];//筷子状态，1表示使用，0表示空闲
PhState phstate[5];//哲学家状态
int timerforPh[5];// 定时器，确定状态变化的时刻
const int SPAN = 91;//定义思考和进食的最长时间
//模拟当i饥饿时，采用的策略。
void hungry(int i)
{
int left = (i)%5;
int right = (i+1)%5;
if(stick[left]==0 && stick[right]==0)
{
stick[left]=stick[right]=1;
phstate[i]=Eating;
timerforPh[i]=rand()%SPAN+1;//设置吃饭时间
}
else
{
phstate[i]=Waiting;
}
}
//从等待中唤醒
void wakeup(int i)
{
//唤醒后的操作同思考时饥饿的操作相同
hungry( i);
}
//模拟吃完后的动作
void ate(int i)
{
stick[(i)%5]=0;
stick[(i+1)%5]=0;
//鍞ら唤醒左右哲学家的顺序可以改成随机的，这里仅仅随机固定顺序
if(phstate[(5+i-1)%5]==Waiting)
wakeup((5+i-1)%5);
if(phstate[(i+1)%5]==Waiting)
wakeup((i+1)%5);
phstate[i]=Thinking;
timerforPh[i]=rand()%SPAN+1;//设置思考时间
}

//输出当前状态，参数为当前时间
void print_state(int cur_time)
{
char state_ch[]={'0','!','X'};
cout.width(4);
cout<<cur_time<<"ms : ";
for(int i=0; i<5; i++)
{
cout.width(2);
cout<<state_ch[phstate[i]]<<' ';
}
cout<<endl;
}

//模拟器
void simulator()
{
//初始化
srand(time(NULL));
for(int i=0; i<5;i++)
{
stick[i]=0;
timerforPh[i]=rand()%SPAN+1;
phstate[i]=Thinking;
}
//模拟开始
long time = 0;//时钟
int eating_event_cnt=0;//进食成功时间次数
while(eating_event_cnt<50)
{
time++;
//检查哪个哲学家的状态需要改变
for(int i=0;i<5;i++)
{
switch(phstate[i])
{
case Waiting:
++timerforPh[i];//等待时间
break;
case Thinking:
if(--timerforPh[i]<0)
{
hungry(i);
print_state(time);
}
break;
default:
if(--timerforPh[i]<0)
{
ate(i);
print_state(time);
eating_event_cnt++;
}
break;
};
}
}
}

int main(int argc, char* argv[])
{
simulator();
return 0;
}
 