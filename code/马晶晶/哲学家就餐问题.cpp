#include <iostream>
#include <iomanip>
#include <ctime>
#include <cstdlib>
using namespace std;
enum State {THINK,WAIT,EAT,END};//哲学家所处状态
bool p(int &a);
bool v(int &a);
class phi//哲学家
{
public:
     phi();
     ~phi();
     void again();//让哲学家回到构造时的状态
     int living(int &r,int &l);//哲学家活着在
     State getsta();//返回哲学家状态
     int getinlet();//得到入口
     void print();//输出状态
private:
     State sta;//哲学家状态
     int inlet;//入口。哲学家应该在做什么？
};
int main()
{
     srand((unsigned)time(NULL));
     phi p[5];//5个哲学家
     int cho[5]={1,1,1,1,1};//5根筷子
     int check1,check2,n=0;char c;
     cout<<"输入y看死锁情况!"<<endl;
     cin>>c;
     while(true)
     {
         int i;
         check1=0;check2=0;//check1检察状态是否全为等待，check2检验是否全部吃完
         for(i=0;i<5;i++)
         {
             if(c=='y')p[i].living(cho[i],cho[(i+4)%5]);
             else{
             if(i%2==0)p[i].living(cho[i],cho[(i+4)%5]);//偶数号先右后左
             else p[i].living(cho[(i+4)%5],cho[i]);//奇数号先左后右
             }
         }
         cout<<setw(8)<<"哲学家：";
         for(i=0;i<5;i++)p[i].print();
         cout<<endl;
         cout<<setw(8)<<"筷子："<<"    ";
         for(i=0;i<5;i++)
         {
             cout<<setw(8)<<cho[i];
             if(p[i].getsta()==WAIT)check1++;
             if(p[i].getinlet()==10)check2++;//等于12时筷子都交公完了！
         }
cout<<endl;
         if(check1==5){
             cout<<"死锁！！！！"<<endl;
             break;
         }
         if(check2==5){
             if(c=='y'){
                 n++;
                 cout<<"第"<<n<<"次..."<<endl;
                 for(i=0;i<5;i++)p[i].again();
             }
             else{
             cout<<"都吃完饭了！！！"<<endl;
             break;
             }
         }
     }
     return 0;
}
bool p(int &a)
{
     if(a==1){
         a--;
         return true;
     }
     else return false;//由于同用一根筷子的人数只有两个，全局看，程序不用设等待队列
}
bool v(int &a)
{
     a++;
     return true;//能交出来，你手上肯定有筷子。
}
phi::phi()
{
     sta=THINK;
     inlet=0;
}
phi::~phi()
{
}
void phi::again()
{
     sta=THINK;
     inlet=0;
}
int phi::living(int &r,int &l)
{
     if(inlet==0){
         int p1=rand()%2;
         if(p1==1)inlet++;//肚子饿了，要开吃了！！！！
     }
     else if(inlet==1){
         if(p(r)){//申请筷子
             sta=WAIT;
             inlet++;//如果分配成功则进入下一阶段，否则等待下次申请
         }
     }
     else if(inlet==2){
         if(p(l)){//申请筷子
             sta=EAT;
             inlet++;//分配成功。进入下一阶段
         }
     }
     else if(inlet>2&&inlet<7){//相当于吃饭需要吃一段时间
         inlet++;
     }
     else if(inlet==7){
         int p2=rand()%2;
         if(p2==1)inlet++;//决定哲学家是否还要断续吃
     }
     else if(inlet==8){
         sta=END;//终于吃完了，交出筷子吧
         v(l);
         inlet++;
     }
     else if(inlet==9){//加到11就执行
         v(r);
         inlet++;
     }
     else ;//??
     return 0;
}
State phi::getsta()
{
     return sta;
}
int phi::getinlet()
{
     return inlet;
}
void phi::print()
{
     if(sta==THINK){cout<<setw(8)<<"思考";}
     else if(sta==WAIT){cout<<setw(8)<<"等待";}
     else if(sta==EAT){cout<<setw(8)<<"用餐";}
     else {cout<<setw(8)<<"吃完";}
}