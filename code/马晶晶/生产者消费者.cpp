#include   <windows.h>   
#include   <iostream>   
const   unsigned   short   SIZE_OF_BUFFER   =   6;   //缓冲区长度   
unsigned   short   ProductNO   =   0;         //产品号   
unsigned   short   ConsumeNO   =   0;         //将被消耗的产品号   
unsigned   short   in   =   0;             //产品进缓冲区时的缓冲区下标   
unsigned   short   out   =   0;             //产品出缓冲区时的缓冲区下标   
    
  int   x_buffer[SIZE_OF_BUFFER];         //缓冲区是个循环队列   
  bool   x_continue   =   true;             //控制程序结束   
  HANDLE   x_hMutex;               //用于线程间的互斥   
  HANDLE   x_hFullSemaphore;           //当缓冲区满时迫使生产者等待   
  HANDLE   x_hEmptySemaphore;           //当缓冲区空时迫使消费者等待   
    
  DWORD   WINAPI   Producer(LPVOID);         //生产者线程   
  DWORD   WINAPI   Consumer(LPVOID);         //消费者线程   
    
  int   main()   
  {   
          //创建各个互斥信号   
          x_hMutex   =   CreateMutex(NULL,FALSE,NULL);   
          x_hFullSemaphore   =   CreateSemaphore(NULL,SIZE_OF_BUFFER-1,SIZE_OF_BUFFER-1,NULL);   
          x_hEmptySemaphore   =   CreateSemaphore(NULL,0,SIZE_OF_BUFFER-1,NULL);   
    
          //调整下面的数值，可以发现，当生产者个数多于消费者个数时，   
          //生产速度快，生产者经常等待消费者；反之，消费者经常等待     
          const   unsigned   short   PRODUCERS_COUNT   =  3;     //生产者的个数   
          const   unsigned   short   CONSUMERS_COUNT   =   2;     //消费者的个数   
    
          //总的线程数   
          const   unsigned   short   THREADS_COUNT   =   PRODUCERS_COUNT+CONSUMERS_COUNT;   
    
          HANDLE   hThreads[PRODUCERS_COUNT];   //各线程的handle   
          DWORD   producerID[CONSUMERS_COUNT];   //生产者线程的标识符  

 
          DWORD   consumerID[THREADS_COUNT];   //消费者线程的标识符   
    
          //创建生产者线程   
          for   (int   i=0;i<PRODUCERS_COUNT;++i)
		  {   
                  hThreads[i]=CreateThread(NULL,0,Producer,NULL,0,&producerID[i]);   
                  if   (hThreads[i]==NULL)  
					  return   -1;   
          }   
          //创建消费者线程   
          for   (int   j=0; j<CONSUMERS_COUNT; ++j)
		  {   
                  hThreads[PRODUCERS_COUNT+j]=CreateThread(NULL,0,Consumer,NULL,0,&consumerID[j]);   
                  if   (hThreads[j]==NULL)   return   -1;   
          }   
    
          while(x_continue)
		  {   
                  if(getchar())
				  {   //按回车后终止程序运行   
                          x_continue   =   false;   
                  }   
          }   
    
          return   0;   
  }   
    
  //生产一个产品。简单模拟了一下，仅输出新产品的NO号   
  void   Produce()   
  {   
      std::cerr   <<   "Producing   "   <<   ++ProductNO   <<   "   ...   ";   
      std::cerr   <<   "Succeed"   <<   std::endl;   
  }   
  
  //把新生产的产品放入缓冲区   
  void   Append()   
  {   
      std::cerr   <<   "放入一个产品   ...   ";   
      x_buffer[in]   =   ProductNO;   
      in   =   (in+1)%SIZE_OF_BUFFER;   
      std::cerr   <<   "Succeed"   <<   std::endl;   
   
      //输出缓冲区当前的状态   
      for   (int   i=0;i<SIZE_OF_BUFFER;++i)
	  {   
          std::cout   <<   i   <<":   "   <<   x_buffer[i];   
          if   (i==in)   
		     std::cout   <<   "   <--   生产";   
          if   (i==out)   
		     std::cout   <<   "   <--   消费";   
             std::cout   <<   std::endl;   
	  }   
  }   
  
  //从缓冲区中取出一个产品   
  void   Take()   
  {   
      std::cerr   <<   "取出一个产品   ...   ";   
      ConsumeNO   =   x_buffer[out];   
      out   =   (out+1)%SIZE_OF_BUFFER;   
      std::cerr   <<   "Succeed"   <<   std::endl;   
   
   //输出缓冲区当前的状态   
      for   (int   i=0;i<SIZE_OF_BUFFER;++i)
	  {   
          std::cout   <<   i   <<":   "   <<   x_buffer[i];   
          if   (i==in)   
			  std::cout   <<   "   <--   生产";   
          if   (i==out)   
			  std::cout   <<   "   <--   消费";   
              std::cout   <<   std::endl;   
	  }
  }
  void   Consume()   
  {   
       std::cerr   <<   "Consuming   "   <<   ConsumeNO   <<   "   ...   ";   
       std::cerr   <<   "Succeed"   <<   std::endl;   
  }   
  
  //生产者   
  DWORD     WINAPI   Producer(LPVOID   lpPara)   
  {   
      while(x_continue)
	  {   
            WaitForSingleObject(x_hFullSemaphore,INFINITE);   
            WaitForSingleObject(x_hMutex,INFINITE);   
            Produce();   
            Append();   
            Sleep(1500);   
            ReleaseMutex(x_hMutex);   
            ReleaseSemaphore(x_hEmptySemaphore,1,NULL);   
	  }   
      return   0;   
  }   
  
  //消费者   
  DWORD     WINAPI   Consumer(LPVOID   lpPara)   
  {   
       while(x_continue)
	   {   
             WaitForSingleObject(x_hEmptySemaphore,INFINITE);   
             WaitForSingleObject(x_hMutex,INFINITE);   
             Take();   
             Consume();   
             Sleep(1500);   
             ReleaseMutex(x_hMutex);   
             ReleaseSemaphore(x_hFullSemaphore,1,NULL);   
	   }   
       return   0;   
  }