#include "StdAfx.h"
#include "Element.h"
#include "Tool.h"


Tool g_ElementTool;

Element::Element(void)
{
	m_left = 0;
	m_length = 73;
	m_pBColor = NULL;
	m_pWnd = NULL;
	m_select = false;
	m_top = 0;
	m_sign = true;
}


Element::~Element(void)
{
}


int Element::SetTop(int top)
{
	int t = m_top;
	m_top = top;
	return t;
}


int Element::SetLeft(int left)
{
	int t = m_left;
	m_left = left;
	return t;
}


int Element::SetLength(int length)
{
	int t = m_length;
	m_length = length;
	return t;
}


CWnd * Element::SetCWnd(CWnd * pWnd)
{
	CWnd * t = m_pWnd;
	m_pWnd = pWnd;
	return t;
}


CBrush * Element::SetCBrush(CBrush * pBColor)
{
	CBrush * t = m_pBColor;
	m_pBColor = pBColor;
	return 0;
}


bool Element::SetSelect(bool select)
{
	bool t = m_select;
	m_select = select;
	return t;
}


void Element::Set(int top, int left, CBrush * pBColor)
{
	SetTop(top);
	SetLeft(left);
	SetCBrush(pBColor);
}


void Element::Show()
{
	CClientDC dc(m_pWnd);
	CPen *pOldPen;
	if(m_select)
	{
		pOldPen=dc.SelectObject(g_ElementTool.fuchsia_pen);
	}
	else
	{
		pOldPen=dc.SelectObject(g_ElementTool.black_pen);
	}

	CBrush *pOldBrush;
	if(m_sign)
	{
		pOldBrush=dc.SelectObject(m_pBColor);
	}
	else
	{
		pOldBrush=dc.SelectObject(g_ElementTool.black_brush);
	}
	dc.Rectangle(m_left, 
				 m_top,
				 m_left+m_length, 
				 m_top+m_length);
	dc.SelectObject(pOldBrush);
	dc.SelectObject(pOldPen);
}

CBrush * Element::GetBColor(void)
{
	return m_pBColor;
}


bool Element::Equal(Element element)
{
	if(m_pBColor == element.m_pBColor)return true;
	return false;
}


bool Element::GetSign(void)
{
	return m_sign;
}


bool Element::SetSign(bool sign)
{
	bool t = m_sign;
	m_sign = sign;
	return t;
}
