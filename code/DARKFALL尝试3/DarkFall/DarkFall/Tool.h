#pragma once
class Tool
{
public:
	Tool(void);
	~Tool(void);
	CPen *black_pen;
	CPen *white_pen;
	CPen *fuchsia_pen;
	CBrush *black_brush;
	CBrush *white_brush;
	CBrush *blue_brush;
	CBrush *green_brush;
	CBrush *red_brush;
	CBrush *yellow_brush;
	CBrush *gray_brush;
	CBrush *buff_brush;
};

