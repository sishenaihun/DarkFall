#pragma once
#include "Element.h"

class Game
{
public:
	Game(void);
	~Game(void);
	int SetTop(int top);
	int SetLeft(int left);
	int SetLength(int length);
	CWnd * SetCWnd(CWnd * pWnd);
	bool SetSelect(bool select);
	void Set(int top, int left);
	void Show();
	CBrush * CreateColor(/*int &i*/);
	void CreateElement(void);
	void OnLButtonDown(CPoint point);

private:
	int m_top;
	int m_left;
	int m_length;
	CWnd * m_pWnd;
	bool m_select;
	int m_lastX;
	int m_lastY;
	Element (*pElement)[8];	

public:
	void Swap(int x, int y, int last_x, int last_y);
	int Checkout(int x, int y, int s);
	int CheckoutX(int x, int y, int s);
	int CheckoutY(int x, int y, int s);
	void Descend(void);
	void Scan(void);
	int Over(void);
};

