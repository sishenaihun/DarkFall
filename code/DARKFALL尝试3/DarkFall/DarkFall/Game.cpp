#include "StdAfx.h"
#include "Game.h"
#include "Tool.h"
#include "Element.h"
#include <time.h>

Tool g_GameTool;

Game::Game(void)
{
	srand((unsigned)time(NULL));

	m_left = 0;
	m_length = 75;
	m_pWnd = NULL;
	m_select = false;
	m_top = 0;
	pElement = NULL;
}


Game::~Game(void)
{
	delete[] pElement;
}


int Game::SetTop(int top)
{
	int t = m_top;
	m_top = top;
	return t;
}


int Game::SetLeft(int left)
{
	int t = m_left;
	m_left = left;
	return t;
}


int Game::SetLength(int length)
{
	int t = m_length;
	m_length = length;
	return t;
}


CWnd * Game::SetCWnd(CWnd * pWnd)
{
	CWnd * t = m_pWnd;
	m_pWnd = pWnd;
	for(int i=0; i<8; i++)
		for(int j=0; j<8; j++)
			pElement[i][j].SetCWnd(m_pWnd);
	return t;
}


bool Game::SetSelect(bool select)
{
	bool t = m_select;
	m_select = select;
	return t;
}


void Game::Set(int top, int left)
{
	SetTop(top);
	SetLeft(left);
}


void Game::Show()
{
	CClientDC dc(m_pWnd);
	CPen *pOldPen=dc.SelectObject(g_GameTool.black_pen);
	dc.Rectangle(m_left-3, 
				 m_top-3,
				 m_left+8*m_length+1, 
				 m_top+8*m_length+1);
	dc.SelectObject(pOldPen);
	for(int i=0; i<8; i++)
		for(int j=0; j<8; j++)
			pElement[i][j].Show();
}


CBrush * Game::CreateColor(/*int &i*/)
{
	switch(/*i =*/ rand()%7)
	{
	case 0:return g_GameTool.buff_brush;
	case 1:return g_GameTool.green_brush;
	case 2:return g_GameTool.blue_brush;
	case 3:return g_GameTool.red_brush;
	case 4:return g_GameTool.yellow_brush;
	case 5:return g_GameTool.white_brush;
	case 6:return g_GameTool.gray_brush;
	}
	return g_GameTool.black_brush;
}

void Game::CreateElement(void)
{
	pElement = new Element[8][8];
	for(int i=0; i<8; i++)
		for(int j=0; j<8; j++)
		{
			//int s;
			CBrush * pBColor = CreateColor();
			while((j > 1
				   && pBColor == pElement[i][j-1].GetBColor()
				   && pBColor == pElement[i][j-2].GetBColor()
				  ) ||
				  (i > 1
				   && pBColor == pElement[i-1][j].GetBColor()
				   && pBColor == pElement[i-2][j].GetBColor()
				  ))
				{
					pBColor = CreateColor();
				}
			
			pElement[i][j].Set(m_top+i*m_length, m_left+j*m_length, pBColor);
		}
	SetCWnd(m_pWnd);
}


void Game::OnLButtonDown(CPoint point)
{
	int x,y;
	if(point.x<m_left || point.x>m_left+8*m_length
	   || point.y<m_top || point.y>m_top+8*m_length)return;

	x = (point.x-m_left)/m_length;
	y = (point.y-m_top)/m_length;

	if(m_select)
	{
		if((x-m_lastX==1 && y==m_lastY)
		   || (x-m_lastX==-1 && y==m_lastY)
		   || (y-m_lastY==1 && x==m_lastX)
		   || (y-m_lastY==-1 && x==m_lastX))
		{
			m_select = false;
			pElement[m_lastY][m_lastX].SetSelect(false);
			Swap(x,y,m_lastX,m_lastY);		

			Show();
			Sleep(400);

			int score1;
			int score2;

			score1 = Checkout(x,y,1);
			score2 = Checkout(m_lastX,m_lastY,1);

			if(score1 == 0 && score2 == 0)  
			{
				Swap(x,y,m_lastX,m_lastY);	//消除判断
			}
			else
			{
				Descend();
				Scan();
				Over();
			}
		}
		else if(x==m_lastX && y==m_lastY)
		{
			m_select = false;
			pElement[m_lastY][m_lastX].SetSelect(false);
		}
		else
		{
			pElement[m_lastY][m_lastX].SetSelect(false);
			m_lastX = x;
			m_lastY = y;
			pElement[y][x].SetSelect(true);
		}
	}
	else
	{
		m_select = true;
		m_lastX = x;
		m_lastY = y;
		pElement[y][x].SetSelect(true);
	}

}


void Game::Swap(int x, int y, int last_x, int last_y)
{
	CBrush * t1 = pElement[last_y][last_x].GetBColor();
	CBrush * t2 = pElement[y][x].GetBColor();
	pElement[y][x].SetCBrush(t1);
	pElement[last_y][last_x].SetCBrush(t2);

	bool b1 = pElement[last_y][last_x].GetSign();
	bool b2 = pElement[y][x].GetSign();
	pElement[y][x].SetSign(b1);
	pElement[last_y][last_x].SetSign(b2);
}




int Game::Checkout(int x, int y, int s)
{
	int numX;
	int numY;

	numX = CheckoutX(x,y,s);
	numY = CheckoutY(x,y,s);

	if(numX >= 3 && numY >= 3)
	{
		return numX+numY-1;
	}
	else if(numX >= 3)
	{
		return numX;
	}
	else if(numY >= 3)
	{
		return numY;
	}

	return 0;
}


int Game::CheckoutX(int x, int y, int s)
{
	int numX = 1;
	int n;
	for(int i=x+1; i<8; i++)
	{
		if(pElement[y][x].Equal(pElement[y][i]))
		{
			numX++;
		}
		else break;
	}
	n = x;
	for(int i=x-1; i>=0; i--)
	{
		if(pElement[y][x].Equal(pElement[y][i]))
		{
			numX++;
			n--;
		}
		else break;
	}
	if(s && numX>=3)
	{
		for(int i=n; i<8; i++)
		{
			if(pElement[y][x].Equal(pElement[y][i]))
			{
				pElement[y][i].SetSign(false);
			}
			else break;
		}
	}
	return numX;
}


int Game::CheckoutY(int x, int y, int s)
{
	int numY = 1;
	int n;
	for(int i=y+1; i<8; i++)
	{
		if(pElement[y][x].Equal(pElement[i][x]))
		{
			numY++;
		}
		else break;
	}
	n = y;
	for(int i=y-1; i>=0; i--)
	{
		if(pElement[y][x].Equal(pElement[i][x]))
		{
			numY++;
			n--;
		}
		else break;
	}
	if(s && numY>=3)
	{
		for(int i=n; i<8; i++)
		{
			if(pElement[y][x].Equal(pElement[i][x]))
			{
				pElement[i][x].SetSign(false);
			}
			else break;
		}
	}
	return numY;
}


void Game::Descend(void)
{
	bool s = true;

	while(s)
	{
		s = false;

		Show();
		Sleep(200);

		for(int i=7; i>0; i--)
			for(int j=7; j>=0; j--)
			{
				if(!pElement[i][j].GetSign())
				{
					s = true;
					Swap(j,i,j,i-1);
				}
			}
		for(int i=7; i>=0; i--)
		{
			if(!pElement[0][i].GetSign())
			{
				pElement[0][i].SetCBrush(CreateColor());
				pElement[0][i].SetSign(true);
			}
		}
	}
}


void Game::Scan(void)
{
	bool s = true;
	while(s)
	{
		s = false;
		for(int i=7; i>=0; i--)
			for(int j=7; j>=0; j--)
			{
				if(Checkout(j,i,1) != 0)
				{
					s = true;
				}
			}

		Sleep(300);
		if(s)Descend();
	}
}


int Game::Over(void)
{
	int i;
	int j;
	int score1;
	int score2;

	i = 0;
	while(i<8)
	{
		j = 0;
		while(j<7)
		{
			Swap(i,j,i,j+1);

			score1 = Checkout(i,j,0);
			score2 = Checkout(i,j+1,0);

			Swap(i,j+1,i,j);

			if(score1 != 0 || score2 != 0)
				break;

			Swap(j,i,j+1,i);

			score1 = Checkout(j,i,0);
			score2 = Checkout(j+1,i,0);

			Swap(j+1,i,j,i);

			if(score1 != 0 || score2 != 0)
				break;

			j++;
		}
		if(score1 != 0 || score2 != 0)break;
		i++;
	}

	if(i == 8 && j == 7)
	{
		MessageBox(NULL,_T("游戏结束!"),_T("提示"),MB_OK | MB_ICONEXCLAMATION);
		if(MessageBox(NULL,_T("是否继续游戏？"),_T("提示"),MB_YESNO | MB_ICONQUESTION) == IDYES)
		{
			delete[] pElement;
			CreateElement();
		}
		else
		{
			PostQuitMessage(0);
		}
	}

	return 0;
}
