
// DarkFallView.cpp : CDarkFallView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "DarkFall.h"
#endif

#include "DarkFallDoc.h"
#include "DarkFallView.h"

#include "Game.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


Game * g_Game;

// CDarkFallView

IMPLEMENT_DYNCREATE(CDarkFallView, CView)

BEGIN_MESSAGE_MAP(CDarkFallView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CDarkFallView::OnFilePrintPreview)
//	ON_WM_CONTEXTMENU()
//	ON_WM_RBUTTONUP()
ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

// CDarkFallView 构造/析构

CDarkFallView::CDarkFallView()
{
	// TODO: 在此处添加构造代码
	
	g_Game = new Game();
	g_Game->Set(50,50);
	g_Game->CreateElement();
	g_Game->SetCWnd(this);	

}

CDarkFallView::~CDarkFallView()
{
	delete g_Game;
}

BOOL CDarkFallView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// CDarkFallView 绘制

void CDarkFallView::OnDraw(CDC* /*pDC*/)
{
	CDarkFallDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 在此处为本机数据添加绘制代码

	g_Game->Show();
}


// CDarkFallView 打印


void CDarkFallView::OnFilePrintPreview()
{
#ifndef SHARED_HANDLERS
	AFXPrintPreview(this);
#endif
}

BOOL CDarkFallView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CDarkFallView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加额外的打印前进行的初始化过程
}

void CDarkFallView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加打印后进行的清理过程
}

//void CDarkFallView::OnRButtonUp(UINT /* nFlags */, CPoint point)
//{
//	ClientToScreen(&point);
//	OnContextMenu(this, point);
//}

//void CDarkFallView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
//{
//#ifndef SHARED_HANDLERS
//	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
//#endif
//}


// CDarkFallView 诊断

#ifdef _DEBUG
void CDarkFallView::AssertValid() const
{
	CView::AssertValid();
}

void CDarkFallView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CDarkFallDoc* CDarkFallView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDarkFallDoc)));
	return (CDarkFallDoc*)m_pDocument;
}
#endif //_DEBUG


// CDarkFallView 消息处理程序


void CDarkFallView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	g_Game->OnLButtonDown(point);
	g_Game->Show();

	CView::OnLButtonDown(nFlags, point);
}
