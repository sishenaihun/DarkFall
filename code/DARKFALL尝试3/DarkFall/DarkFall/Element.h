#pragma once
class Element
{
public:
	Element(void);
	~Element(void);
	int SetTop(int top);
	int SetLeft(int left);
	int SetLength(int length);
	CWnd * SetCWnd(CWnd * pWnd);
	CBrush * SetCBrush(CBrush * pBColor);
	bool SetSelect(bool select);
	void Set(int top, int left, CBrush * pBColor);
	void Show();


private:
	int m_top;
	int m_left;
	int m_length;
	CWnd * m_pWnd;
	CBrush * m_pBColor;
	bool m_select;
	bool m_sign;


public:
	CBrush * GetBColor(void);
	bool Equal(Element element);
	bool GetSign(void);
	bool SetSign(bool sign);
};

