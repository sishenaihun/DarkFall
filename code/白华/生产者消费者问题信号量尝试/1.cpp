#include <stdio.h>
#include <windows.h>
#include <process.h> 

struct Box
{
	int n;	//库存
	int max;//容量
};

HANDLE g_hSemaphore;
//CRITICAL_SECTION g_cs;

unsigned int __stdcall Produce(LPVOID param)
{
	
	struct Box *p=(struct Box*)param;

	int n;

	while(true)
	{
		WaitForSingleObject(g_hSemaphore, INFINITE);
		//EnterCriticalSection(&g_cs);
		
		if(p->n < p->max)
		{
			n=1;
			Sleep(50);
			printf("生产%3d物品，现在库存%3d。\n", n, p->n = p->n + n);

		}

		//LeaveCriticalSection(&g_cs);
		ReleaseSemaphore(g_hSemaphore, 1, NULL);
		Sleep(200);
	}

	return 0;
}

unsigned int __stdcall Consume(LPVOID param)
{
	struct Box *p=(struct Box*)param;

	int n;

	while(true)
	{
		WaitForSingleObject(g_hSemaphore, INFINITE);
		//EnterCriticalSection(&g_cs);
		
		if(p->n > 0)
		{
			n=1;
			Sleep(50);			
			printf("消费%3d物品，现在库存%3d。\n", n, p->n = p->n - n);

		}

		//LeaveCriticalSection(&g_cs);
		ReleaseSemaphore(g_hSemaphore, 1, NULL);
		Sleep(200);
	}	

	return 0;
}

int main()
{
	struct Box box={0,500};

	HANDLE handle1,handle2;

	g_hSemaphore = CreateSemaphore(NULL, 1, 1, NULL);
	//InitializeCriticalSection(&g_cs);

	handle1 = (HANDLE)_beginthreadex(NULL,0,Produce,&box,0,NULL);
	handle2 = (HANDLE)_beginthreadex(NULL,0,Consume,&box,0,NULL);

	system("pause");

	return 0;
}